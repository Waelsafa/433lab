package com.example.waelsafa.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.database.MergeCursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.example.waelsafa.myapplication.R.*;


//TODO Make something useful

public class MainActivity extends AppCompatActivity {
    Button button1;
    Button button2;
    Button button3;
    ListView lv;
    TextView txt;
    TextInputLayout txtssn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SQLiteDatabase mydatabase = openOrCreateDatabase("lab8",MODE_PRIVATE,null);

        //Create the Tables
        createTables(mydatabase);

        //Fill The Tables
        insertValues(mydatabase);



        setContentView(layout.activity_main);

        //Button Declarations
        button1 = (Button) findViewById(id.button1);
        button2=(Button) findViewById(id.button2);
        button3=(Button)findViewById(id.button3);

        //ListView Declaration
        lv=(ListView) findViewById(id.listview);




        //TODO List of all employees with roles

        //TODO Add Ability to insert



    }

    public void AddEntry(View v){
            Intent i = new Intent(this, AddNewField.class);
            startActivity(i);
            finish();

        }




    public void DisplayAllDept(View v){

        SQLiteDatabase db = openOrCreateDatabase("lab8", SQLiteDatabase.CREATE_IF_NECESSARY, null);

        lv = (ListView) findViewById(id.listview);

        String query1 = "SELECT * FROM Department ";

        Cursor c = db.rawQuery(query1, null);
        int size = c.getCount();
        if (size != 0) {
            c.moveToFirst();
            String[] i = new String[size];
            int count = 0;
            while (count < size) {
                i[count] = "Department name: "+c.getString(c.getColumnIndex("dname")) + "\nDepartment code " +
                        c.getString(c.getColumnIndex("dcode")) + "\nOffice_number: " +
                        c.getString(c.getColumnIndex("office_number")) +"\nOffice_pnumber: "+
                        c.getString(c.getColumnIndex("office_pnumber")) +"\nFaculty: "+
                        c.getString(c.getColumnIndex("faculty"))+
                        "\n";
                count++;
                c.moveToNext();
            }

            c.close();

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, i);
            lv.setAdapter(adapter);
            db.close();
        }





    }

    public void DisplayGrades(View v){

        SQLiteDatabase db = openOrCreateDatabase("lab8", SQLiteDatabase.CREATE_IF_NECESSARY, null);

        lv = (ListView) findViewById(id.listview);

        String query1 = "SELECT * FROM Student ";

        Cursor c = db.rawQuery(query1, null);
        int size = c.getCount();
        if (size != 0) {
            c.moveToFirst();
            String[] i = new String[size];
            int count = 0;
            while (count < size) {
                i[count] = "Student SSN: "+c.getString(c.getColumnIndex("SSN")) + "\nStudent Snum: " +
                        c.getString(c.getColumnIndex("snum")) + "\n Gender " +
                        c.getString(c.getColumnIndex("Gender")) +"\nStreet Adress: "+
                        c.getString(c.getColumnIndex("street_adress")) +"\n City: "+
                        c.getString(c.getColumnIndex("City"))+ "\n State: " +
                        c.getString(c.getColumnIndex("State"))+ "\n ZIP: " +
                        c.getString(c.getColumnIndex("ZIP"))+ "\n Padress: " +
                        c.getString(c.getColumnIndex("Padress"))+ "\n Class: " +
                        c.getString(c.getColumnIndex("Class"))+ "\n Degree program: " +
                        c.getString(c.getColumnIndex("Degree_program"))+ "\n Birthdate: " +
                        c.getString(c.getColumnIndex("Bdate"))+ "\n Phone Number :" +
                        c.getString(c.getColumnIndex("Phone_number"))+ "\n First Name : " +
                        c.getString(c.getColumnIndex("Fname"))+ "\n Last Name: " +
                        c.getString(c.getColumnIndex("LName"))+
                        "\n";
                count++;
                c.moveToNext();
            }

            c.close();

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, i);
            lv.setAdapter(adapter);
            db.close();
        }





    }





    public void createTables(SQLiteDatabase mydatabase){

        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Department ( dname VARCHAR(10) NOT NULL, dcode INT NOT NULL, office_number INT NOT NULL, office_pnumber INT NOT NULL, faculty VARCHAR(10) NOT NULL, PRIMARY KEY (dcode), UNIQUE (dname) );\n");

        mydatabase.execSQL("\n" +
                "CREATE TABLE IF NOT EXISTS Student (\n" +
                "Ssn INT NOT NULL,\n" +
                "Snum INT NOT NULL,\n" +
                "Gender VARCHAR(10) NOT NULL, street_address VARCHAR(10) NOT NULL, City VARCHAR(10) NOT NULL,\n" +
                "State VARCHAR(10) NOT NULL,\n" +
                "Zip INT NOT NULL,\n" +
                "Paddress VARCHAR(10) NOT NULL,\n" +
                "Class VARCHAR(10) NOT NULL, Degree_program VARCHAR(10) NOT NULL, Bdate DATE NOT NULL,\n" +
                "Phone_number VARCHAR(10) NOT NULL, FName VARCHAR(10) NOT NULL,\n" +
                "LName VARCHAR(10) NOT NULL, PRIMARY KEY (Snum),\n" +
                "UNIQUE (Ssn)\n" +
                ");");

        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS MinorsIn (\n" +
                "Snum INT NOT NULL,\n" +
                "dcode INT NOT NULL,\n" +
                "FOREIGN KEY (Snum) REFERENCES Student(Snum), FOREIGN KEY (dcode) REFERENCES Department(dcode)\n" +
                ");");

        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS MajorsIn (\n" +
                "Snum INT NOT NULL,\n" +
                "dcode INT NOT NULL,\n" +
                "FOREIGN KEY (Snum) REFERENCES Student(Snum), FOREIGN KEY (dcode) REFERENCES Department(dcode)\n" +
                ");");

        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Course (\n" +
                "cname VARCHAR(10) NOT NULL, description VARCHAR(10) NOT NULL, cnumber INT NOT NULL, semester_hours INT NOT NULL,\n" +
                "level INT NOT NULL,\n" +
                "department INT NOT NULL,\n" +
                "PRIMARY KEY (cnumber),\n" +
                "FOREIGN KEY (department) REFERENCES Department(dcode)\n" +
                ");");

        mydatabase.execSQL("  CREATE TABLE IF NOT EXISTS Section (\n" +
                "instructor VARCHAR(10) NOT NULL, semester VARCHAR(10) NOT NULL, section_number INT NOT NULL, year DATE NOT NULL,\n" +
                "course INT NOT NULL,\n" +
                "PRIMARY KEY (section_number, course),\n" +
                "FOREIGN KEY (course) REFERENCES Course(cnumber)\n" +
                ");");

        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS RegisteredIn (\n" +
                "numeric_grade FLOAT NOT NULL, letter_grade VARCHAR(10) NOT NULL, Snum INT NOT NULL,\n" +
                "section_number INT NOT NULL, cnumber INT NOT NULL,\n" +
                "FOREIGN KEY (Snum) REFERENCES Student(Snum),\n" +
                "FOREIGN KEY (section_number, cnumber) REFERENCES Section(section_number, course) );");


    }

    public void insertValues(SQLiteDatabase mydatabase){


        mydatabase.execSQL("Insert or replace into Student VALUES (115, 201502235, 'male', 'street3', 'baabda', 'state1', 21211, 'paddress2', 'senior', 'BEng', '1994-04-30', '71651345', 'Chris', 'Saad');");
        mydatabase.execSQL("Insert or replace into Student VALUES (114, 201400000, 'male', 'street2', 'chiyeh', 'state2', 21211, 'paddress1', 'freshman', 'BEng', '1995-03-30', '71666645', 'Hussein', 'Safa');");

        mydatabase.execSQL("Insert or replace into  Course VALUES ('Databases', 'nicecourse', 433, 15, 4, 123);");
        mydatabase.execSQL("Insert or replace INTO Department VALUES ('PHIL', 113, 546, 05211333, 'Philosophy');");
        mydatabase.execSQL("Insert or replace INTO Department VALUES ('ECON', 443, 812, 05228883, 'Business');");
    }


}
